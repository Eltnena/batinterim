<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace chBiBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
/**
 * Description of EntrepreneurController
 *
 * @author developpeur
 */
class EntrepreneurController extends Controller
{
    public function accueilAction()
    {
        return $this->render('@chBi/Entrepreneur/accueilEntrepreneur.html.twig');
    }
}
