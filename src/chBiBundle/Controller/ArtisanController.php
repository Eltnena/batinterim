<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace chBiBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
/**
 * Description of ArtisanController
 *
 * @author developpeur
 */
class ArtisanController extends Controller
{
    public function accueilAction()
    {
        return $this->render('@chBi/Artisan/accueilArtisan.html.twig');
    }
}
