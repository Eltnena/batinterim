<?php

namespace chBiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($profil)
    {
        return $this->render('@chBi/Default/index.html.twig');
    }
}
